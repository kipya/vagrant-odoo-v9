#!/usr/bin/env bash

echo " --- Updating packages list --- "
apt-get update

# Base Packages
# -------------
echo " --- Installing Base Packages --- "
apt-get install -y vim wget curl python-software-properties aptitude

echo " --- Configuring Base Packages --- "
echo -e "syntax on\nset number\ncolorscheme koehler" > /root/.vimrc
cp /root/.vimrc /home/vagrant/.vimrc

# Remove unneeded packages
# ------------------------
echo " --- Removing unneeded packages --- "
#apt-get remove -y nano

#echo " --- Updating packages list --- "
apt-get update


echo " --- Installing Server Security --- "
apt-get install -y ufw unattended-upgrade heirloom-mailx logwatch libdate-manip-perl tiger 

# Mysql
echo " --- Configuring MySQL --- "
sed -i 's/symbolic-links=0/symbolic-links=0\nbind-address=0.0.0.0/g' /etc/mysql/my.cnf


# MySQL
# -----
echo " --- Configuring MySQL ---"
echo " user: 'user', password: '3C(7>ghfj{e3T2', database: 'demo'"
echo "FLUSH PRIVILEGES" | mysql
echo "CREATE DATABASE IF NOT EXISTS demo" | mysql
echo "CREATE USER 'user'@'localhost' IDENTIFIED BY '3C(7>ghfj{e3T2'" | mysql
echo "GRANT ALL PRIVILEGES ON demo.* TO 'user'@'localhost' IDENTIFIED BY '3C(7>ghfj{e3T2'" | mysql
echo "FLUSH PRIVILEGES" | mysql


# echo " --- Upgrading packages list --- "
apt-get upgrade -y